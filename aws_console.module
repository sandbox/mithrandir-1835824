<?php
/**
 * @file
 * Provides primary Drupal hook implementations.
 *
 * @author Jan Keller Catalan ("Mithrandir", http://drupal.org/user/15431)
 */

/**
 * Implements hook_menu().
 */
function aws_console_menu() {
  $items['admin/structure/aws_console'] = array(
    'title' => 'AWS Console',
    'page callback' => 'aws_console_overview',
    'access arguments' => array('Access AWS Console'),
    'type' => MENU_NORMAL_ITEM,
  );
  $items['admin/config/media/awssdk/console'] = array(
    'title' => 'Management Console',
    'description' => 'Configure Amazon Web Services Management Console settings',
    'page callback' => 'drupal_get_form',
    'page arguments' => array('aws_console_settings_form'),
    'access arguments' => array('Access AWS Console'),
  );

  return $items;
}

/**
 * Stub function so far, meant to list the various AWS services enabled through submodules.
 */
function aws_console_overview() {
  return '';
}

/**
 * Settings form.
 */
function aws_console_settings_form($form, &$form_state) {
  $form['required'] = array(
    '#type' => 'fieldset',
    '#title' => t('Required'),
    '#description' => t('The following fields are required by the deployment process.'),
  );
  $region_options = array(
    'REGION_US_E1' => t('United States East (Northern Virginia) Region'),
    'REGION_US_W1' => t('United States West (Northern California) Region'),
    'REGION_US_W2' => t('United States West (Oregon) Region'),
    'REGION_EU_W1' => t('Europe West (Ireland) Region'),
    'REGION_APAC_NE1' => t('Asia Pacific Northeast (Tokyo) Region'),
  );
  $form['required']['aws_console_region'] = array(
    '#type' => 'select',
    '#title' => t('Amazon Web Services Region'),
    '#default_value' => variable_get('aws_console_region', ''),
    '#required' => TRUE,
    '#description' => t('Amazon Web Services region to use for deployments.'),
    '#options' => $region_options,
  );
  return system_settings_form($form);
}
