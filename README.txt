-- SUMMARY --

AWS Console is meant to be a helper module for managing Amazon Web Services.
It is NOT meant to replace the existing Management Console from Amazon, but
instead provide an overview of certain information and automating certain tasks
through submodules.

For a full description of the module, visit the project page:
  http://drupal.org/sandbox/Mithrandir/1835824

To submit bug reports and feature suggestions, or to track changes:
  http://drupal.org/project/issues/1835824


-- REQUIREMENTS --

* AWS SDK module

-- INSTALLATION --

* Install as usual, see 
  http://drupal.org/documentation/install/modules-themes/modules-7
  for further information.


-- CONFIGURATION --

* Configure user permissions in Administration » People » Permissions:

  - Access AWS EB

    Users with a role that has the "Access AWS Console" permission will be able 
    to set AWS Console settings.


* Set the default settings in Administration » Configuration » Media » AWS SDK
  » Management Console.
