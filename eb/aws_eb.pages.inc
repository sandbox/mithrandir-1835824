<?php
/**
 * @file
 * Pages and form definitions.
 *
 * @author Jan Keller Catalan ("Mithrandir", http://drupal.org/user/15431)
 */

/**
 * Show an overview of Elastic Beanstalk applications available.
 */
function aws_eb_overview() {
  libraries_load('awssdk');
  $eb = new AmazonElasticBeanstalk();
  $eb->set_region(constant("AmazonElasticBeanstalk::" . variable_get('aws_console_region', 'REGION_IRELAND')));
  $applications = $eb->describe_applications()->body->DescribeApplicationsResult->Applications->member;
  $rows = array();
  foreach ($applications as $thisapplication) {
    $application = $thisapplication->to_array()->getArrayCopy();
    $rows[] = array(
      'name' => l($application['ApplicationName'], 'admin/structure/aws_console/eb/' . $application['ApplicationName']),
      'description' => @$application['Description'],
      'updated' => $application['DateUpdated'],
    );
  }
  return theme('table', array('header' => array_keys($rows[0]), 'rows' => $rows));
}

/**
 * Show an overview of an application's environments, resources and versions.
 *
 * @param string $name
 *   Name of application
 *
 * @return string
 *   HTML
 */
function aws_eb_application($name) {
  drupal_add_css(drupal_get_path('module', 'aws_eb') . '/aws_eb.css');

  libraries_load('awssdk');
  $eb = new AmazonElasticBeanstalk();
  $eb->set_region(constant("AmazonElasticBeanstalk::" . variable_get('aws_console_region', 'REGION_IRELAND')));

  $application = $eb->describe_applications(array('ApplicationNames' => $name))->body->DescribeApplicationsResult->Applications->member;
  $versions = $eb->describe_application_versions(array('ApplicationName' => $name))->body->DescribeApplicationVersionsResult->ApplicationVersions->member;
  $environments = $eb->describe_environments(array('ApplicationName' => $name))->body->DescribeEnvironmentsResult->Environments->member;

  $rows = array($application[0]->to_array()->getArrayCopy());
  unset($rows[0]['Versions'], $rows[0]['ConfigurationTemplates']);
  $output = theme('table', array('header' => array_keys($rows[0]), 'rows' => $rows));

  $rows = array();
  foreach ($environments as $environment) {
    if ($environment->Status == "Terminated") {
      continue;
    }
    $row['Health'] = '<div class="aws_eb_environment_health ' . $environment->Health . '"></div>';
    $row['Status'] = $environment->Status;
    $row['CName'] = $environment->CNAME;
    $row['EnvironmentName'] = $environment->EnvironmentName;
    $row['VersionLabel'] = $environment->VersionLabel;
    $row['Description'] = isset($environment->Description) ? $environment->Description : '';
    $row['EnvironmentId'] = $environment->EnvironmentId;
    $row['DateCreated'] = $environment->DateCreated;

    $resources = $eb->describe_environment_resources(array('EnvironmentId' => $environment->EnvironmentId));
    if (isset($resources->body->DescribeEnvironmentResourcesResult->EnvironmentResources->Instances)) {
      $row['Instances'] = count($resources->body->DescribeEnvironmentResourcesResult->EnvironmentResources->Instances->member);
    }
    else {
      $row['Instances'] = 0;
    }
    $rows[] = $row;

    $event = $eb->describe_events(array('EnvironmentId' => $environment->EnvironmentId, 'MaxRecords' => 1));
    $last_event = $event->body->DescribeEventsResult->Events->member->Message;
    $rows[] = array('cell' => array('data' => $last_event, 'colspan' => count($row)));
  }
  $output .= theme('table', array('header' => array_keys($rows[0]), 'rows' => $rows));

  $rows = array();
  foreach ($versions as $version) {
    $row = $version->to_array()->getArrayCopy();
    unset($row['SourceBundle'], $row['ApplicationName']);
    $row['Source'] = l($version->SourceBundle->S3Key, "https://" . $version->SourceBundle->S3Bucket . ".s3.amazonaws.com/" . $version->SourceBundle->S3Key);
    $rows[] = $row;
  }
  $output .= theme('table', array('header' => array_keys($rows[0]), 'rows' => $rows));

  return $output;
}

/**
 * Deployment form for an Amazon Beanstalk application, selecting version and environment to deploy to.
 */
function aws_eb_deploy_form($form, $form_state, $name) {
  if (isset($form_state['storage']['confirm_needed'])) {
    return aws_eb_deploy_confirm($form_state);
  }

  libraries_load('awssdk');
  $eb = new AmazonElasticBeanstalk();
  $eb->set_region(constant("AmazonElasticBeanstalk::" . variable_get('aws_console_region', 'REGION_IRELAND')));

  $environment_options = array();
  $environments = $eb->describe_environments(array('ApplicationName' => $name))->body->DescribeEnvironmentsResult->Environments->member;
  foreach ($environments as $environment) {
    if ($environment->Status == 'Ready') {
      $environment_options[(string) $environment->EnvironmentId] = (string) $environment->EnvironmentName;
    }
  }

  $versions = $eb->describe_application_versions(array('ApplicationName' => $name))->body->DescribeApplicationVersionsResult->ApplicationVersions->member;
  $version_options = array('new' => '<New Version>');
  foreach ($versions as $version) {
    $version_options[(string) $version->VersionLabel] = (string) $version->VersionLabel;
  }

  $s3 = new AmazonS3();
  $s3->set_region(constant("AmazonS3::" . variable_get('aws_console_region', 'REGION_IRELAND')));
  $s3_buckets = $s3->list_buckets();
  $bucket_options = array();
  foreach ($s3_buckets->body->Buckets->Bucket as $bucket) {
    $bucket_options[(string) $bucket->Name] = (string) $bucket->Name;
  }

  $form['application_name'] = array(
    '#type' => 'value',
    '#value' => $name,
  );
  $form['environment_id'] = array(
    '#type' => 'select',
    '#title' => 'Environment',
    '#description' => 'Select environment to deploy to',
    '#options' => $environment_options,
  );
  $form['version_label'] = array(
    '#type' => 'select',
    '#title' => 'Version label',
    '#description' => 'Select which version to deploy',
    '#options' => $version_options,
  );
  $form['s3_container'] = array(
    '#type' => 'container',
    '#states' => array(
      'visible' => array(
        'select#edit-version-label' => array('value' => 'new'),
      ),
    ),
  );
  $form['s3_container']['s3_bucket'] = array(
    '#type' => 'select',
    '#title' => 'S3 Bucket',
    '#description' => 'S3 bucket to upload new application version to',
    '#options' => $bucket_options,
  );
  $form['zero_downtime'] = array(
    '#type' => 'checkbox',
    '#title' => 'Deploy with zero downtime',
    '#description' => 'If enabled, a new environment will be deployed and the CNAME switched with the selected environment',
    '#default_value' => TRUE,
  );
  $form['submit'] = array(
    '#type' => 'submit',
    '#value' => 'Deploy',
  );
  return $form;
}

/**
 * Confirmation form for aws_eb_deploy_form to guard against erroneous deployments.
 */
function aws_eb_deploy_confirm(&$form_state) {
  libraries_load('awssdk');
  $eb = new AmazonElasticBeanstalk();
  $eb->set_region(constant("AmazonElasticBeanstalk::" . variable_get('aws_console_region', 'REGION_IRELAND')));

  $environment = $eb->describe_environments(array('EnvironmentId' => $form_state['values']['environment_id']))->body->DescribeEnvironmentsResult->Environments->member;

  $form = array();
  $form['application_name'] = array(
    '#type' => 'value',
    '#value' => $form_state['values']['application_name'],
  );
  $form['version_label'] = array(
    '#type' => 'value',
    '#value' => $form_state['values']['version_label'],
  );
  $form['environment_id'] = array(
    '#type' => 'value',
    '#value' => $form_state['values']['environment_id'],
  );
  $form['s3_bucket'] = array(
    '#type' => 'value',
    '#value' => $form_state['values']['s3_bucket'],
  );
  $form['zero_downtime'] = array(
    '#type' => 'value',
    '#value' => $form_state['values']['zero_downtime'],
  );

  $question = t(
    'Are you sure to deploy %version_label to %environment_name on %application_name',
    array(
      '%version_label' => $form_state['values']['version_label'],
      '%environment_name' => $environment->EnvironmentName,
      '%application_name' => $form_state['values']['application_name'],
    )
  );
  $path = isset($_GET['destination']) ? $_GET['destination'] : '<front>';
  $yes = t('Deploy');

  return confirm_form($form, $question, $path, '', $yes);
}

/**
 * Submit aws_eb_deploy_form() or ask for confirmation.
 *
 * Starts a background process for the actual deployment.
 *
 * @see _aws_eb_deploy()
 */
function aws_eb_deploy_form_submit(&$form, &$form_state) {
  // Redirects to confirmation form if not confirmed.
  if (!isset($form_state['values']['confirm'])) {
    $form_state['storage'] = array(
      'confirm_needed' => TRUE,
    );
    $form_state['rebuild'] = TRUE;
    return;
  }

  $application_name = $form_state['values']['application_name'];
  $version_label = $form_state['values']['version_label'];
  $environment_id = $form_state['values']['environment_id'];
  $options = array(
    'zero_downtime' => $form_state['values']['zero_downtime'],
    's3_bucket' => $form_state['values']['s3_bucket'],
  );

  // Run the job in background.
  $result = NULL;
  $handle = "aws_eb:" . $application_name . '.' . $environment_id;
  $process = new BackgroundProcess($handle);

  $result = $process->start(
    '_aws_eb_deploy',
    array(
      $application_name,
      $version_label,
      $environment_id,
      $options,
    )
  );

  if ($result) {
    drupal_set_message(t('Process %process started', array('%process' => $process->connection)));
  }
  else {
    drupal_set_message(t('Deploy process could not be started'), 'error');
  }
  drupal_goto('admin/structure/aws_console/eb/' . $application_name);
}
