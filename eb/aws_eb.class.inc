<?php
/**
 * @file
 * Provides primary Drupal hook implementations.
 *
 * @author Jan Keller Catalan ("Mithrandir", http://drupal.org/user/15431)
 */

class AwsEb {
  /**
   * AmazonElasticBeanstalk SDK object
   */
  protected $eb = NULL;
  /**
   * Name of the AWS application
   */
  protected $applicationName = '';
  /**
   * Label of new version
   */
  protected $versionLabel = '';
  /**
   * Environment to deploy to
   */
  protected $environmentId = '';
  /**
   * Internally used name of the configuration template
   */
  protected $templateName = '';
  /**
   * Constructor. Sets up delegation object.
   *
   * @param string $application_name
   *   (Required) The name of the application.
   * @param string $version_label
   *   (Required) A label identifying this version.
   * @param string $environment_id
   *   (Required) ID of the environment to deploy to.
   * @param array $options
   *   (Optional) An associative array of parameters that can have the following keys:
   * 	 <ul><li><code>region</code> - <code>string</code> - Optional - Region for the Elastic Beanstalk application</li></ul>
   *
   * @see AmazonElasticBeanstalk
   */
  public function __construct($application_name, $version_label, $environment_id, $options = array()) {
    libraries_load('awssdk');
    if (!isset($options['region'])) {
      $options['region'] = constant("AmazonElasticBeanstalk::" . variable_get('aws_console_region', 'REGION_IRELAND'));
    }

    $this->versionLabel = $version_label;
    $this->applicationName = $application_name;
    $this->environmentId = $environment_id;
    $this->templateName = $this->applicationName . '-conftemplate';

    $this->eb = new AmazonElasticBeanstalk();
    $this->eb->set_region($options['region']);
  }

  /**
   * Deploys an application version for the specified application.
   *
   * <p class="note">
   * Once you create an application version with a specified Amazon S3 bucket and key location, you
   * cannot change that Amazon S3 location. If you change the Amazon S3 location, you receive an
   * exception when you attempt to launch an environment from the application version.
   * </p>
   *
   * @param array $options
   *   (Optional) An associative array of parameters that can have the following keys:
   *   <ul><li><code>wait_time</code> - <code>string</code> - Optional - How long to wait in seconds for a new environment to turn GREEN. Method throws an Exception if it takes longer than this time to turn on the new environment</li></ul>
   */
  public function deploy($options = array()) {
    if (!isset($options['wait_time'])) {
      $options['wait_time'] = variable_get('aws_eb_deploy_wait_time', 1000);
    }
    if (!isset($options['zero_downtime'])) {
      $options['zero_downtime'] = TRUE;
    }

    if ($this->versionLabel == 'new') {
      // Create new application version.
      $this->createNewVersion($options);
    }

    // Check your current environment to make sure it is Green and Ready.
    $environments = $this->eb->describe_environments(array('EnvironmentId' => $this->environmentId));
    if (!$environments->body->DescribeEnvironmentsResult->Environments->member->Health == 'Green') {
      watchdog(
        'aws_eb',
        'Environment %environment has status: %status',
        array(
          '%environment' => $this->environmentId,
          '%status' => $environments->body->DescribeEnvironmentsResult->Environments->member->Health,
        ),
        WATCHDOG_ERROR
      );
      return;
    }

    // Check that the application version exists.
    if (!$this->versionExists()) {
      watchdog(
        'aws_eb',
        'Version @version_label does not exist for application @application',
        array(
          '@version_label' => $this->versionLabel,
          '@application' => $this->applicationName,
        ),
        WATCHDOG_ERROR
      );
      return;
    }

    if ($options['zero_downtime']) {
      // $this->createConfigurationTemplate(); // This bugs out with a signature mismatch error - unclear why, so just use the same configuration as the existing environment.
      $this->templateName = $environments->body->DescribeEnvironmentsResult->Environments->member->TemplateName;

      // Determine new environment name based on the one, it is replacing.
      $current_environment_array = explode('-', $environments->body->DescribeEnvironmentsResult->Environments->member->EnvironmentName);
      if (count($current_environment_array) > 1) {
        array_pop($current_environment_array);
      }
      $current_environment_array[] = date('YmdHi');

      $new_environment_name = implode('-', $current_environment_array);
      $new_environment_id = $this->launchNewEnvironment($new_environment_name);
      if ($new_environment_id) {
        $this->swapCnames($this->environmentId, $new_environment_id, $options['wait_time']);
      }
      else {
        return;
      }
    }
    else {
      $this->eb->update_environment(
        array(
          'EnvironmentId' => $this->environmentId,
          'VersionLabel' => $this->versionLabel,
          'Description' => 'Automatically deployed through Drupal aws_console',
        )
      );
    }
  }

  /**
   * Create a new application version from local git repository.
   */
  protected function createNewVersion($options = array()) {
    global $user;
    $this->versionLabel = $this->applicationName . '-' . date('YmdHi');
    $s3_key = $this->uploadNewVersion($options['s3_bucket']);

    $eb = new AmazonElasticBeanstalk();
    $eb->set_region(constant("AmazonElasticBeanstalk::" . variable_get('aws_console_region', 'REGION_IRELAND')));

    $app_version = $eb->create_application_version($this->applicationName, $this->versionLabel,
      array(
        'Description' => 'Automatically generated version using PHP AWS SDK. Uploaded by ' . $user->name . ' (' . $user->uid . ')',
        'SourceBundle' => array(
          'S3Bucket' => $options['s3_bucket'],
          'S3Key' => $s3_key,
        ),
      )
    );
  }

  /**
   * Update local repository, zip it and upload to S3.
   */
  protected function uploadNewVersion($s3_bucket) {
    // Update git repository.
    $olddir = getcwd();
    chdir(variable_get('aws_eb_git_directory', '/home/vagrant/drupal-kd'));
    exec("git submodule init 2>&1", $output);
    exec("git submodule update 2>&1", $output);
    exec("git pull 2>&1", $output);

    // Zip repository to a temp file.
    $temp_file = tempnam(sys_get_temp_dir(), $this->applicationName) . '.zip';
    exec("zip -9 -r --exclude=*.git* --exclude=sites/default/files $temp_file . 2>&1", $output);
    chdir($olddir);

    // Upload zip file to S3.
    $s3 = new AmazonS3();
    $s3->set_region(constant("AmazonS3::" . variable_get('aws_console_region', 'REGION_IRELAND')));
    $response = $s3->create_object(
      $s3_bucket,
      $this->versionLabel . '.zip',
      array(
        'fileUpload'  => $temp_file,
      )
    );
    if ($response->status == 200) {
      return $this->versionLabel . '.zip';
    }
  }

  /**
   * Create a configuration template from the existing environment.
   */
  protected function createConfigurationTemplate() {
    $results = $this->eb->create_configuration_template($this->applicationName, $this->templateName, array('EnvironmentId', $this->environmentId));
    $template_info = $results->body->CreateConfigurationTemplateResult;
    if (!$template_info) {
      watchdog(
        'aws_eb',
        'Could not create configuration template @template for application @application in environment @environment',
        array(
          '@template' => $this->templateName,
          '@application' => $this->applicationName,
          '@environment' => $this->environmentId,
        ),
        WATCHDOG_ERROR
      );
    }
  }

  /**
   * Launch new environment based on the existing configuration.
   *
   * @return string
   *   ID of the newly launched environment
   * @throws Exception
   *   if the new environment could not be launched
   */
  protected function launchNewEnvironment($new_environment_name) {
    // Launch a new environment for the new application version and template.
    $results = $this->eb->create_environment(
      $this->applicationName,
      $new_environment_name,
      array(
        'VersionLabel' => $this->versionLabel,
        'TemplateName' => $this->templateName,
        'Description' => 'Automatically deployed through Drupal aws_console',
      )
    );
    $new_environment_info = $results->body->CreateEnvironmentResult;
    if (!$new_environment_info) {
      watchdog(
        'aws_eb',
        'Could not launch new environment on template @template for application @application in environment @environment. Error code @error_code - @message',
        array(
          '@template' => $this->templateName,
          '@application' => $this->applicationName,
          '@environment' => $new_environment_name,
          '@error_code' => $results->body->Error->Code,
          '@message' => $results->body->Error->Message,
        ),
        WATCHDOG_ERROR
      );
    }

    return $new_environment_info->EnvironmentId;
  }

  /**
   * Swap CNAMEs for two environments.
   *
   * @param string $current_environment
   *   ID of the currently running environment
   * @param string $new_environment_id
   *   ID of the newly created environment
   */
  protected function swapCnames($current_environment, $new_environment_id, $wait_time) {
    $start = time();
    $done = FALSE;
    do {
      $results = $this->eb->describe_environments(array('EnvironmentIds' => $new_environment_id));
      if ($results->body->DescribeEnvironmentsResult->Environments->member->Health == 'Green') {
        // After the new environment is Green and Ready, swap the environment CNAMEs.
        $swap_response = $this->eb->swap_environment_cnames(array('SourceEnvironmentId' => $current_environment, 'DestinationEnvironmentId' => $new_environment_id));

        do {
          $results = $this->eb->describe_environments(array('EnvironmentIds' => $new_environment_id));
          // After it is confirmed that the environment CNAME swap has completed, terminate the old environment.
          if ($results->body->DescribeEnvironmentsResult->Environments->member->Health == 'Green') {
            $this->cleanUp($current_environment);
            $done = TRUE;
          }
          sleep(5);
        } while (!$done);
      }
      else {
        if (time() - $start > $wait_time) {
          // Something went wrong, abort.
          $this->cleanUp($new_environment_id);
          $done = TRUE;
          watchdog('aws_eb', 'Environment %environment_id never became healthy', array('%environment_id' => $new_environment_id), WATCHDOG_ERROR);
        }
        // Let's give it a little more time.
        sleep(10);
      }
    } while (!$done);
  }

  /**
   * Check whether the application version exists.
   *
   * @return bool
   *   TRUE if application version exists.
   */
  protected function versionExists() {
    $version_result = $this->eb->describe_application_versions(
      array(
        'ApplicationName' => $this->applicationName,
        'VersionLabels' => $this->versionLabel,
      )
    );
    return $version_result->body->DescribeApplicationVersionsResult->ApplicationVersions->member;
  }

  /**
   * Clean up unneeded Amazon resources.
   *
   * @param string $environment_id
   *   ID of the environment to terminate.
   */
  protected function cleanUp($environment_id) {
    // Terminate environment.
    $terminate_response = $this->eb->terminate_environment(array('EnvironmentId' => $environment_id));
    if ($terminate_response->status == 200) {
      watchdog('aws_eb', 'Environment %environment is being terminated', array('%environment' => $environment_id), WATCHDOG_INFO);
    }

    // Remove configuration template // Awaiting fix for configuration template creation. Disabled until then.
    // $delete_response = $this->eb->delete_configuration_template($this->applicationName, $this->templateName);
  }
}
