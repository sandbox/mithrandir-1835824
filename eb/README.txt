-- SUMMARY --

AWS Elastic Beanstalk helps automate some of the tasks involved in deploying
code to Amazon Web Services.

For a full description of the module, visit the project page:
  http://drupal.org/sandbox/Mithrandir/1835824

To submit bug reports and feature suggestions, or to track changes:
  http://drupal.org/project/issues/1835824


-- REQUIREMENTS --

* Amazon Web Services Console
* Background Process module for deploying in the background (lengthy process)


-- INSTALLATION --

* Install as usual, see 
  http://drupal.org/documentation/install/modules-themes/modules-7
  for further information.


-- CONFIGURATION --

* Configure user permissions in Administration » People » Permissions:

  - Access AWS EB

    Users with a role that has the "Access AWS EB" permission will be able to
    see information about the current AWS setup and deploy new versions of code
    to the environments.


* Set the default settings in Administration » Configuration » Media » AWS SDK
  » Management Console.
